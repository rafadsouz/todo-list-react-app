# Use uma imagem base com Node.js
FROM node:14

# Defina o diretório de trabalho dentro do contêiner
WORKDIR /src

# Copie os arquivos do projeto para o contêiner
COPY . .

# Instale as dependências do projeto
RUN npm install

# Construa o aplicativo React
RUN npm run build

# Expõe a porta onde a aplicação React irá rodar
EXPOSE 3000

# Inicialize o aplicativo quando o contêiner for iniciado
CMD ["npm", "start"]
